```python linenums='1'
def nb_occurrences(phrase):
    d_occurrences = {}
    for caractere in phrase:
        if caractere in d_occurrences:
            d_occurrences[caractere] += 1
        else:
            d_occurrences[caractere] = 1
    return d_occurrences
```

Ou encore avec la méthode `get` pour éviter le `if` explicite (**hors-programme**) :

```python
def nb_occurrences(phrase):
    d_occurrences = {}
    for caractere in phrase:
        d_occurrences[caractere] = d_occurrences.get(caractere, 0) + 1
    return d_occurrences
```

Construction en compréhension et fonction prédéfinie de Python (**hors-programme**) :

```python
def nb_occurrences(phrase):
    return {lettre:phrase.count(lettre) for lettre in phrase}
```