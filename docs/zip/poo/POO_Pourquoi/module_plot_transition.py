import csv
from posix import listdir
import re
import random
from os import read 
import matplotlib.pyplot as pl


class CountryDemographicTransition:

    """ This class handles the data from a given country and format it to be displayed """
    def __init__(self, name):
        self.country = name
        self.color = (random.random(), random.random(), random.random())
        self.get_data()
        
    def get_specific_country_data(self, fname, country):

        def read_csv_file(fname):
            with open(fname, newline='') as csvfile:
                reader = csv.DictReader(csvfile, delimiter = ";")
                return [row for row in reader]

        def extract_specific_country(fname, country):
            all = read_csv_file(fname)
            for i, line in enumerate(all):
                if country in line['Country Name']:
                    return i, line
            raise Exception('Not a valid country.')

        line_no, line = extract_specific_country(fname, country)
        values, years = [], []
        for key, value in line.items():
            if re.search("[0-9]{4}", key):
                if value != '':
                    years.append(float(key))
                    values.append(float(value))
        return years, values

    def get_data(self):
        self.years, self.birth = self.get_specific_country_data('birth_rate.csv', self.country)
        self.years, self.death = self.get_specific_country_data('death_rate.csv', self.country)

    def create_birth(self):
        pl.plot(self.years, self.birth, color = self.color)

    def create_death(self):
        pl.plot(self.years, self.death, '--', color = self.color)

class Graph:

    """ This class handles the drawing of several demographic transitions using Matplotlib. """
    def __init__(self, list):
        self.list_country = list

        demographics = [CountryDemographicTransition(country) for country in self.list_country]
        for country in demographics:
            country.create_birth()
        for country in demographics:
            country.create_death()
        pl.legend(self.list_country)
        pl.show()  

if __name__ == "__main__":
    Graph(['France', 'Japan', 'Italy', 'United Kingdom'])
