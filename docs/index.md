# Spécialité NSI en Terminale

## Introduction

Le but de ce site web est de vous proposer les corrections des exercices que nous avons fait en cours. Il permet de retravailler les notions à partir des énoncés d'origine. 

Tant que l'exercice ne demande pas d'utilisation graphique, vous pouvez même programmer en Python directement en ligne sans avoir besoin d'installer un éditeur !

Je vous conseille d'utiliser le navigateur **Firefox**, **Chrome** ou **Chromium** afin de bénéficier de la meilleure expérience utilisateur.

Ce site respecte votre vie privée : aucun cookie n'est installé ; aucune inscription n'est requise.

## Fonctionnement des exercices

!!! example "Programmer"

    Vous devez compléter ou écrire un programme dans un éditeur. 

    - Vous pouvez tester en appuyant sur la flèche pointant à droite ▶️. 
    - Vous pouvez tenter de valider votre programme pour savoir si celui-ci est correct en cliquant sur le gendarme 🛂. Votre programme est alors soumis à de nombreux tests. 
    - Au bout de 5 validations ratées, la solution apparait.

    {{IDE('exo2')}}

## FAQ

Voici quelques questions que ous pourriez vous poser :

!!! help "Rien ne s'enregistre et lorsque je recharge la page internet, tout s'efface !"

    C'est normal. Il n'y a pas de cookie ou de sessions. Vos données ne sont donc pas enregistrées.

    Vous pouvez toutefois télécharger vos programmes lorsque ceux-ci sont importants.

!!! help "C'est normal que je n'arrive pas à faire un copier/coller de certains codes du cours ?"

    Oui. J'ai bloqué cette fonctionnalité. La programmation s'apprend en programmant.

!!! help "Il faut vraiment TOUT savoir ce qu'il y a sur votre site ?"

    Non. Avec le  contrôle de cours, je m'assure que vous avez travaillé le cours avant de venir. Sinon, vous perdez votre temps et celui de vos camarades.

!!! help "Un contrôle de cours par semaine, ça sert à rien et ça fait perdre du temps."

    Non. Avec un contrôle de cours, je m'assure que vous avez travaillé le cours avant de venir. Sinon, vous perdez votre temps et celui de vos camarades.

!!! help "C'est sympa de pouvoir coder directement sur une page web. Qui est responsable de cela ?"

    C'est moi qui ait développé tout le moteur. Cela fonctionne grâce à une technologie de 2017 appelé WebAssembly. Celle-ci permet de coupler Javascript et Python. Et d'autres développements arrivent...