Exercice délicat nous révèle G.Connan :

- une coquille dans le résultat du deuxième exemple : la réponse est `[42]`  et non `42`.
- il s'agit de créer une liste à partir des éléments de la liste initiale ; si on ne s'y prend pas comme il faut, les modifications en place par exemple vont conduire à des désastres.

Source : [commentaires à propos du 03.1](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md#exercice-31)