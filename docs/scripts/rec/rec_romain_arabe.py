VALEUR_ROMAIN = { 'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1, '' : 0}

def romain_to_arabe(nombre : str) -> int:
    """ Convertit récursivement un nombre en écriture romaine 
    en son équivalent en écriture arabe"""
    pass


def romain_to_arabe_itératif(nombre : str) -> int:
    """ Convertit de manière itérative un nombre en écriture romaine 
    en son équivalent en écriture arabe"""
    pass