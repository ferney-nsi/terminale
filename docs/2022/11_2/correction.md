```python linenums='1'
ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'

def position_alphabet(lettre):
    return ord(lettre) - ord('A')

def cesar(message, decalage):
    resultat = ''
    for caractere in message:
        if caractere in ALPHABET:
            indice = (position_alphabet(caractere) + decalage) % 26
            resultat = resultat + ALPHABET[indice]
        else:
            resultat = resultat + caractere
    return resultat
```