# Pourquoi la Programmation Orientée Objet ?

## Exemple

- [ ] Télécharger le fichier [joint](zip/poo/POO_Pourquoi.zip)
- [ ] Dézipper le fichier
- [ ] Ouvrir fichier_utilisateur.py . Comprenez-vous ce qu'il fait ? Pouvez-vous l'utiliser rapidement ?
- [ ] Ouvrir module_plot_transition.py . Comprenez-vous ce qu'il fait ? Pouvez-vous l'utiliser rapidement ?

## Jeu de plateau

Télécharger le fichier [joint](scripts/poo/jeu_plateau_POO.py)