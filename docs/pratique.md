# Structures de données

## TD Pile

Télécharger le fichier [joint](scripts/structures/TD_Pile_Eleve.py)

## TD File

Télécharger le fichier [joint](scripts/structures/TP_File_Eleve.py)

## TD Arbres de Huffman

Télécharger le fichier [joint](scripts/structures/huffman_eleve.py)

Télécharger le fichier intermédiaire [joint](scripts/structures/huffman_prof_inter.py)