def syracuse(u):
    print(u)
    if u == 1: # cas d'arrêt
        return True
    if u % 2 == 0:
        return syracuse(u // 2)  # calcul de u_{n+1}
    else:
        return syracuse(3 * u + 1) # calcul de u_{n+1}

def syracuse_vol(u, temps_de_vol = 0):
    """ on veut mesurer le temps de vol"""
    print(u, temps_de_vol)
    if u == 1: # cas d'arrêt
        return temps_de_vol
    # temps_de_vol += 1
    if u % 2 == 0:
        return syracuse_vol(u // 2, temps_de_vol + 1)
    else:
        return syracuse_vol(3 * u + 1, temps_de_vol + 1)
