Écrire une fonction `consecutifs_successifs` qui prend en paramètre un tableau de nombres entiers
`tab`, et qui renvoie la liste (éventuellement vide) des couples d'entiers consécutifs
successifs qu'il peut y avoir dans `tab`.

Exemples :
```python
>>> consecutifs_successifs([1, 4, 3, 5])
[]
>>> consecutifs_successifs([1, 4, 5, 3])
[(4, 5)]
>>> consecutifs_successifs([7, 1, 2, 5, 3, 4])
[(1, 2), (3, 4)]
>>> consecutifs_successifs([5, 1, 2, 3, 8, -5, -4, 7])
[(1, 2), (2, 3), (-5, -4)]
```