On considère des personnes, identifiées par un pseudo unique, qui peuvent s'envoyer des messages
avec deux règles à respecter :

- chaque personne ne peut envoyer des messages qu'à 1 seule et même personne
(éventuellement elle-même),
- chaque personne ne peut recevoir des messages qu'en provenance d'une seule
personne (éventuellement elle-même).

Voici un exemple - avec 6 personnes - de « plan d'envoi des messages » qui respecte les
règles ci-dessus, puisque chaque personne est présente une seule fois dans chaque
colonne :

- Anne envoie ses messages à Elodie
- Elodie envoie ses messages à Bruno
- Bruno envoie ses messages à Fidel
- Fidel envoie ses messages à Anne
- Claude envoie ses messages à Denis
- Denis envoie ses messages à Claude

Et le dictionnaire correspondant à ce plan d'envoi est le suivant :

```python
plan_a = {'Anne':'Elodie', 'Bruno':'Fidel', 'Claude':'Denis', 
    'Denis':'Claude', 'Elodie':'Bruno', 'Fidel':'Anne'}
```

Sur le plan d'envoi `plan_a` des messages ci-dessus, il y a deux cycles distincts : un premier
cycle avec Anne, Elodie, Bruno, Fidel et un second cycle avec Claude et Denis. En revanche, le plan d’envoi `plan_b` ci-dessous :

```python
plan_b = {'Anne':'Claude', 'Bruno':'Fidel', 'Claude':'Elodie', 
        'Denis':'Anne', 'Elodie':'Bruno', 'Fidel':'Denis'}
```

comporte un unique cycle : Anne, Claude, Elodie, Bruno, Fidel, Denis. Dans ce cas, lorsqu’un plan d’envoi comporte un unique cycle, on dit que le plan d’envoi est *cyclique*.

Pour savoir si un plan d'envoi de messages comportant N personnes est cyclique, on peut
utiliser l'algorithme ci-dessous :


On part d'une personne A et on inspecte les N – 1 successeurs dans le plan d'envoi :

- Si un de ces N – 1 successeurs est A lui-même, on a trouvé un cycle de taille
inférieure ou égale à N – 1. Il y a donc au moins deux cycles et le plan d'envoi n'est
pas cyclique.

- Si on ne retombe pas sur A lors de cette inspection, on a un unique cycle qui passe
par toutes les personnes : le plan d'envoi est cyclique.


Compléter la fonction suivante en respectant la spécification.

*Remarque :* la fonction python `len` permet d'obtenir la longueur d'un dictionnaire.


```python linenums='1'
def est_cyclique(plan, pseudo):
    '''
    Prend en paramètre un dictionnaire plan correspondant
    à un plan d'envoi de messages entre des personnes
    ainsi qu'un pseudo faisant partie de ce plan 
    Renvoie True si le plan d'envoi de messages est cyclique
    et False sinon.
    '''
    n = len(plan)
    personne = ...
    for i in range(n-1):
        ... = plan[...]
        if personne == ...:
            return ...
    return ...
```

*Exemples :*

```python
>>> PLAN_A = {'Anne':'Elodie', 'Fidel':'Anne', 'Claude':'Denis', 'Elodie':'Bruno', 'Bruno':'Fidel', 'Denis':'Claude'}
>>> est_cyclique(PLAN_A, 'Anne')
False
>>> PLAN_B = {'Anne':'Elodie', 'Fidel':'Claude', 'Claude':'Denis', 'Elodie':'Bruno', 'Bruno':'Fidel', 'Denis':'Anne'}
>>> est_cyclique(PLAN_B, 'Anne')
True
>>> est_cyclique(PLAN_B, 'Denis')
True
>>> PLAN_C = {'Anne':'Bruno', 'Fidel':'Claude', 'Claude':'Denis', 'Elodie':'Anne', 'Bruno':'Fidel', 'Denis':'Elodie'}
>>> est_cyclique(PLAN_C, 'Claude')
True
```