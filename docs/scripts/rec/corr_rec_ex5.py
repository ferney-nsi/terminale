def etoiles(n: int):
    print('*' * n)

def affiche_vers_bas(n: int):
    if n == 0:
        return
    etoiles(n)
    affiche_vers_bas(n-1)

def affiche_vers_haut(n: int):
    if n == 0:
        return
    affiche_vers_haut(n-1)
    etoiles(n)

affiche_vers_bas(4)