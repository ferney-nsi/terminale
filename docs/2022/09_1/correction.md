```python
def syracuse(n):
    suite = []
    while n > 1:
        suite.append(n)
        if n % 2 == 0:
            n = n // 2
        else:
            n = 3 * n + 1
    suite.append(1)
    return suite
```

