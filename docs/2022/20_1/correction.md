```python
def xor(bits_a, bits_b):
    n = len(bits_a)
    assert n == len(bits_b)
    bits_xor = []
    for i in range(n):
        if bits_a[i] != bits_b[i]:
            bits_xor.append(1)
        else:
            bits_xor.append(0)
    return bits_xor
```

Une version en compréhension, plus élégante (légèrement hors-programme peut-être à cause du _cast_ en _int_ du booléen) :

```python
def xor(bits_a, bits_b):
    n = len(bits_a)
    assert n == len(bits_b)
    return [int(bits_a[i] != bits_b[i]) for i in range(n)]
```

Si on connait les opérateurs bit à bit de Python :

```python
def xor(bits_a, bits_b):
    n = len(bits_a)
    assert n == len(bits_b)
    return [bits_a[i] ^ bits_b[i] for i in range(n)]
```

Ou alors on ruse :

```python
def xor(bits_a, bits_b):
    n = len(bits_a)
    assert n == len(bits_b)
    return [abs(bits_a[i] - bits_b[i]) for i in range(n)]
```
