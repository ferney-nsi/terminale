VALEUR_ROMAIN = { 'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1, '' : 0}

def romain_to_arabe(nombre : str) -> int:
    """ Convertit récursivement un nombre en écriture romaine 
    en son équivalent en écriture arabe"""
    if len(nombre) <= 1: 
        return VALEUR_ROMAIN[nombre]  # le nombre fait une taille 0 ou 1

    if len(nombre) >=2 and VALEUR_ROMAIN[nombre[0]] < VALEUR_ROMAIN[nombre[1]]: 
        return romain_to_arabe(nombre[2:]) + VALEUR_ROMAIN[nombre[1]] - VALEUR_ROMAIN[nombre[0]]
    else:
        return romain_to_arabe(nombre[1:]) + VALEUR_ROMAIN[nombre[0]]


def romain_to_arabe_itératif(nombre : str) -> int:
    """ Convertit de manière itérative un nombre en écriture romaine 
    en son équivalent en écriture arabe"""
    valeur = 0
    i = 0
    while i < len(nombre)-1:
        if VALEUR_ROMAIN[nombre[i]] < VALEUR_ROMAIN[nombre[i+1]]:
            valeur += VALEUR_ROMAIN[nombre[i+1]] - VALEUR_ROMAIN[nombre[i]]
            i += 2
        else: 
            valeur += VALEUR_ROMAIN[nombre[i]]
            i += 1
    if i == len(nombre)-1:
        valeur += VALEUR_ROMAIN[nombre[i]]
    return valeur

print(romain_to_arabe('MMMXX'))
print(romain_to_arabe_itératif('MMMXX'))
