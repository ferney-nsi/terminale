Q. Konieczko pointe pas mal de point délicats pour la compréhension de l'énoncé de cet exercice : [commentaires à propos du 20.1](https://mooc-forums.inria.fr/moocnsi/t/bns-dautres-erreurs-encore-non-pointees/3745/12) :

- le caractère peu lisible utilisé pour le "ou exclusif"
- la mauvais présentation de la table de vérité, peu lisible
- les exemples de tableaux de bits non nécessaires là où ils se trouvent

Effectivement, le caractère ⊕ n'apporte rien et on devrait remplacer la ligne par une vraie table de vérité.

C'est le premier sujet où les exemples sont présentés dans des `assert`. Par souci d'harmonisation il serait bon de ne pas le faire.