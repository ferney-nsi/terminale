Soit `tableau` un tableau non vide d'entiers triés dans l'ordre croissant et `n` un entier.
La fonction `indice_de` doit renvoyer l'indice où la valeur `n`
apparaît dans `tableau`, et `None` si la valeur n'y figure pas. 

Cette recherche utilise une fonction `chercher` récursive et basée sur le principe « diviser pour
régner ».

Les paramètres de la fonction `chercher` sont :

- `tableau`, le tableau dans lequel s'effectue la recherche ;
- `n`, l'entier à chercher dans le tableau ;
- `deb`, l'indice de début de la partie du tableau où s'effectue la recherche ;
- `fin`, l'indice de fin de la partie du tableau où s'effectue la recherche.

Recopier et compléter le code de la fonction `chercher` proposée ci-dessous :

```python linenums='1'
def indice_de(tableau, element):
    return chercher(tableau, element, 0, len(tableau)-1)

def chercher(tableau, n, deb, fin):
    if deb > fin:
        ...
    milieu = ...
    if tableau[milieu] < ... :
        return chercher(tableau, n, ..., ...)
    elif ... :
        return chercher(tableau, n, ..., ...)
    else :
        return ...
```

L'exécution du code doit donner :
```python
>>> indice_de([1, 5, 6, 6, 9, 12], 7)
>>> indice_de([1, 5, 6, 6, 9, 12], 9)
4
>>> indice_de([1, 5, 6, 6, 9, 12], 6)
2
```