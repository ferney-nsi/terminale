G.Connan s'est chargé de la relecture du sujet 01. Voici son verdict :

> Pas grand chose mis à part  cette distinction caractère/chaîne de caractères qui
n'existe  pas en  Python. La  personne qui  a rédigé  est sûrement  une habituée
d'Ocaml en prépa :)
Il y  a aussi cette espace  après la virgule  des appels de recherche  plus ou
moins existante...
Enfin le choix du  nom `recherche` est discutable car cela  ferait plus penser à
un test renvoyant un booléen (je recherche `i` dans `mississippi`). On aurait pu
choisir `compte_lettres`, `nb_occurrences`, `compte_occurrences`, etc.

(source : [commentaires à propos du 01.1](https://gitlab.com/nsind/tale/-/blob/master/docs/DIVERS/bns22.md))

Je le rejoins sur l'ensemble des points.