import tkinter as tk
import time

class Carré:

    """Crée un carré d'un plateau
    
    Paramètres:
    vec -- position des sommets du polygone, format [(x0,y0), (x1,y1), (x2,y2), (x3,y3), ]
    texte -- texte à afficher sur le carré
    """

    def __init__(self, vec, texte = ''):
        self.texte = texte
        self.liste = []  # liste représente la liste des nombres autorisés dans le sudoku
        self.coord = vec

    def changer_texte(self, texte):
        """Met à jour le texte d'un carré"""
        self.texte = texte

    def changer_liste(self, texte):
        """Met à jour le texte d'un carré"""
        self.liste = texte

    def calculer_text_coord(self):
        """Méthode générale : affiche un texte unique"""
        x0, y0 = self.coord[0]
        x1, y1 = self.coord[2]
        return ((x0 + x1)/2, (y0 +y1)/2)

    def calculer_multi_text_coord(self, margins = -2):
        """Méthode spécifique au Sudoku : renvoie les coordonnées d'une liste de texte à afficher dans le carré.
        
        Paramètres:
        margins -- taille des marges sur le côté entre le texte et le bord du carré"""
        x0, y0 = self.coord[0]
        x1, y1 = self.coord[2]
        n = len(self.liste)
        if n >= 5:
            return [(x0+ abs(x0-x1)/4 + i % 3 * abs(x0-x1)/4, y0 + abs(y0-y1)/4 + i // 3 * abs(y0-y1)/4) for i in range(n)]
        else:
            return [(x0 + margins + abs(x0 + margins-(x1-margins))/3 + i % 2 * abs(x0 + margins-(x1-margins))/3, \
                     y0 + margins + abs(y0 + margins-(y1-margins))/3 + i // 2 * abs(y0 + margins-(y1-margins))/3) for i in range(n)]

class Damier(tk.Tk):

    """Créer un plateau constitué de Carré, hérite des méthodes de Tk()
    
    Paramètres:
    plateau -- un tableau de tableau de 9x9 contenant des nombres de 0 à 9.
    """

    def __init__(self, plateau, L = 50):
        super().__init__()
        self.plateau = plateau
        self.canvas = tk.Canvas(self, width=470, height=470) 

        motif_base = [(10, 10), (10 + L, 10), (10 + L, 10 + L), (10, 10 + L)] # motif de base à répéter pour tous les carrés du damier.
        # tableau de tableau de Carré constituant le damier final.
        self.carrés = [[Carré([(x + L * i, y + L * j) for (x,y) in motif_base], self.plateau[i][j] if self.plateau[i][j] != 0 else '') for i in range(9)] for j in range(9)]
    
    def _main_lines(self, L = 50):
        """Dessine la grille du Sudoku"""
        self.canvas.delete(self.canvas.find_withtag(f"grid"))
        lignes_verticales = [[x0 * L + 10, 10, x0 * L + 10, 10 + L * 9] for x0 in range(10)]
        lignes_horizontales = [[10, x0 * L + 10, 10 + L * 9, x0 * L + 10] for x0 in range(10)]
        for j in range(10):
            if j%3 == 0:
                self.canvas.create_line(lignes_verticales[j], width=2, tags = 'grid')
                self.canvas.create_line(lignes_horizontales[j], width=2, tags = 'grid')

    def afficher_plateau(self, L = 50):
        """Affiche le plateau en parcourant self.carrés, le tableau de tableau de Case"""
        for j in range(9):
            for i in range(9):
                self.canvas.create_polygon(self.carrés[i][j].coord, fill='', outline='black')
                self.canvas.create_text(self.carrés[j][i].calculer_text_coord(), text= self.carrés[i][j].texte, fill = 'red', font=("Times", 20))
        self._main_lines()
        self.canvas.grid(row=0, column=0)


    def maj_plateau(self, i, j, p, chiffres_possibles, t_sleep = 0.6):
        """ Met à jour l'affichage du plateau
        
        Paramètres :
        i, j -- indices de la case concernée par le changement actuel
        p -- valeur modifiée
        chiffres_possibles -- ensemble ( set() ) des chiffres autorisés
        t_sleep -- rafraichissement de la fenêtre graphique. 
        Si t_sleep == 0, pas de mise à jour.
        
        Remarque :
        En backtracking, l'animation prend deux fois plus de temps.
        """
        if t_sleep != 0:
            self.canvas.delete(self.canvas.find_withtag(f"text_{i}{j}"))
            self.canvas.delete(self.canvas.find_withtag(f"square_{i}{j}"))
            self.canvas.create_polygon(self.carrés[i][j].coord, fill='black', outline='black', tags = f'square_{i}{j}')
            if p == 0: 
                # self.canvas.create_polygon(self.carrés[i][j].coord, fill='black', outline='black')
                time.sleep(2 * t_sleep)
            else:
                self.carrés[i][j].changer_liste(list(chiffres_possibles))
                coords = self.carrés[i][j].calculer_multi_text_coord()

                for k, chiffres in enumerate(chiffres_possibles):
                    if chiffres != p:
                        self.canvas.create_text(coords[k], text= self.carrés[i][j].liste[k], font=("Times", 18), tags= f"text_{i}{j}")
                    else:
                        self.canvas.create_text(coords[k], text= self.carrés[i][j].liste[k], fill ="green", font=("Times", 20), tags= f"text_{i}{j}")
                time.sleep(t_sleep)
            self._main_lines()
            self.canvas.update()


L = [[0, 9, 0, 2, 0, 0, 6, 0, 5], [3, 2, 0, 0, 0, 7, 0, 0, 0], [0, 7, 0, 9, 0, 5, 0, 0, 8],[0,1,0,0,0,0,0,0,0],[0,0,7, 0,0,0,0,9,4],[6,0,0,0,0,0,0,0, 0],[0,0,8,0,0,0,0,0,7],[0,3,0, 4, 9, 1, 5, 0, 0], [0, 0, 0, 0, 0, 3, 0, 0, 0]]

def chiffres_ligne(L, i):
    pass

def chiffres_colonne(L, i):
    pass

def chiffres_bloc(L, i, j):
    
    def calculer_numéro_bloc(i, j):
        """fonction calculant le numéro du bloc 3x3 dans lequel appartient le carré aux coordonnées (i, j) """
        pass

    pass

def calculer_chiffres_autorisés(L, i, j):
    pass

def case_suivante(i, j):
    pass

def solution_sudoku(L):
    dam = Damier(L)
    dam.afficher_plateau()

    def aux(i, j):
        pass

    return aux(0, 0)

if __name__ == "__main__":

    print(chiffres_ligne(L, 0))
    print(chiffres_colonne(L, 0))
    print(chiffres_bloc(L, 4, 5))
    print(calculer_chiffres_autorisés(L, 0, 0))
    print(case_suivante(8,8))

    # Bouton permettant de lancer la résolution du Sudoku et l'affichage.
    tk.Button(text = "Démarrer", command = lambda x = 2 : solution_sudoku(L)).pack()

    tk.mainloop()