# TP 1 - Conversion nombre arabe

## Énoncé

On suppose défini le dictionnaire :

`#!python VALEUR_ROMAIN = { 'M' : 1000, 'D' : 500, 'C' : 100, 'L' : 50, 'X' : 10, 'V' : 5, 'I' : 1, '' : 0}`

Le but est de convertir un nombre en écriture romaine en son équivalent en écriture arabe.

Attention : il est nécessaire de prendre en compte le cas où la valeur correspondante au second caractère est supérieure à celle du premier.

!!! example "Exemple"

    ```python
    >>> romain_to_arabe('X')
    10
    >>> romain_to_arabe('XCI')
    91
    >>> romain_to_arabe('MMXIX')
    2019
    ```

- [ ] Réaliser une fonction récursive `#!python romain_to_arabe(nombre : str) -> int` qui prend en paramètre une chaîne de caractères représentant un "nombre romain" et dont le résultat est l’entier correspondant.

- [ ] Réaliser une fonction `#!python romain_to_arabe_itératif(nombre : str) -> int` sans récursivité. Comparer ensuite les deux approches.

## Exercice

{{IDE('rec/rec_romain_arabe')}}