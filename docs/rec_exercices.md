# Exercices sur la récursivité

!!! exo "Exercices"

    === {{exercice(False, 0)}}

        Soit  la suite d'entiers définie par : $u_{n+1}=\left \{ \begin{align} \frac{u_n}{2}  & {\rm \quad si}\  u_n \rm{ \ est \ pair}\\ 3u_n +1 & {\rm \quad si}\  u_n \rm{ \ est \ impair}\\ \end{align} \right .$ avec $u_0$ un entier quelconque plus grand que 1.

        - [ ] Écrire une fonction récursive `#!python syracuse(u)` qui affiche les valeurs successives de la suite $u_n$ tant que $u_n$ est différent de 1 et renvoie `#!python True` quand la suite arrive à 1.
        - [ ] On souhaite que notre fonction renvoie le temps de vol (i.e. le nombre d'étapes avant d'atteindre 1 pour la première fois). Ajouter une variable temps_de_vol dont la valeur sera incrémentée de 1 à chaque appel à syracuse. Sa valeur initiale sera égale à 0.

        {{IDE('rec/rec_ex1')}}
    
    === {{exercice(False)}}

        Écrire une fonction récursive `#!python mult(a: float, b: int)` qui renvoie le résultat de la multiplication du flottant a par l'entier b. On se rappellera que $\rm a\times b = a+a+a+ \ldots +a$, où l'addition est répétée b fois.

        {{IDE('rec/rec_ex2')}}

    === {{exercice(False, 4)}}

        On dispose d'une fonction `#!python étoiles` permettant d'afficher sur la même ligne n étoiles : 
        ```python
        def etoiles(n):
            print('*' * n)
        ```

        - [ ] Écrire une fonction récursive `#!python affiche_vers_bas(n: int)` qui réalise l'affichage ci-dessous.
        - [ ] Écrire une fonction récursive `#!python affiche_vers_haut(n: int)` qui réalise l'affichage ci-dessous. Pensez à la pile d'exécution !


        ???+ tip "Affichage à obtenir"

            ```python
            >>> affichage_vers_bas(3)
            ***
            **
            *
            >>> affichage_vers_haut(3)
            *
            **
            ***
            ```

        {{IDE('rec/rec_ex5')}}

    === {{exercice(False, 7)}}

        Écrire une fonction récursive appartient(v, t, i) prenant en paramètres un entier recherché v, un tableau t et un indice de tableau i. Cette fonction renvoie True si v apparait dans t entre l'indice i inclus et len(t) exclu, et False sinon. On suppose que i est compris en 0 et len(t) exclus.

        ??? tip "Exemple"

            ```python
            >>> appartient(3, [4, 5, 6, 3, 2, 1], 1)  # 3 est-il dans la liste [5, 6, 3, 2, 1]
            True
            >>> appartient(3, [4, 5, 6, 3, 2, 1], 4)  # 3 est-il dans la liste [2, 1]
            False
            ```

        {{IDE('rec/rec_ex8')}}
