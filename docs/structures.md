# Structures de données

## TD Pile

Télécharger le fichier [joint](scripts/structures/TD_Pile_Eleve.py)

## TD File

Télécharger le fichier [joint](scripts/structures/TP_File_Eleve.py)

## TD Arbres de Huffman

Télécharger le fichier [joint](scripts/structures/huffman_eleve.py)

Télécharger le fichier intermédiaire [joint](scripts/structures/huffman_prof_inter.py)

## Graphes 

Fichier de visualisation [joint](scripts/structures/GViz.py)

Code GrapheDico (si vous avez perdu le votre) [joint](scripts/structures/graph_Eleve.py)

## TP 1 Graphes : Echelle de mots

Fichier [echelle.py](scripts/structures/echelle.py) à compléter

Librairie [GViz](scripts/structures/GViz.py)

Classe [GrapheDico](scripts/structures/grapheDico.py) corrigée

## TP 2 Graphes : Coloriage

Librairie [GViz_coloriage](scripts/structures/GViz_coloration.py)

Classe [Graphe_coloration](scripts/structures/Graphe_coloration.py)
