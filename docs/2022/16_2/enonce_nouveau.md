Cet exercice utilise des piles qui seront représentées en Python par des listes (type `list`). De plus, lors de l'affichage de la pile en ligne, le côté droit représente le sommet de la pile.

On rappelle que l’expression `liste_1 = list(liste)` fait une copie de `liste` indépendante de `liste`. L’expression `x = pile.pop()` enlève le sommet de la pile `pile` et le place dans la variable `x`. Enfin, l’expression `pile.append(v)` place la valeur `v` au sommet de la pile `pile`.

Compléter le code Python de la fonction `positifs` ci-dessous qui prend une pile `pile` de
nombres entiers en paramètre et qui renvoie la pile des entiers positifs dans le même
ordre, en laissant inchangée la pile référencée par la variable `pile`.

```python linenums='1'
def positifs(pile):
    pile2 = ...(pile)
    pile3 = ...
    while pile2 != []:
        x = ...
        if ... >= 0:
            pile3.append(...)
    while pile3 != ...:
        ... .append(...)
    return pile2
```

Exemples :
```python
>>> T = [-1, 0, 5, -3, 4, -6, 10, 9, -8]
>>> positifs(T)
[0, 5, 4, 10, 9]
>>> T
[-1, 0, 5, -3, 4, -6, 10, 9, -8]

>>> T = [-10, -5]
>>> positifs(T)
[]
>>> T
[-10, -5]

>>> T = [1, 2, 3, 4]
>>> positifs(T)
[1, 2, 3, 4]
>>> T
[1, 2, 3, 4]
```