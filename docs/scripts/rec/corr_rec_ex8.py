def appartient(v: int, t: list[int], i: int):
    if i >= len(t):
        return False
    if v == t[i]:
        return True
    else:
        return appartient(v, t, i + 1)

def appartient_optimisé(v: int, t: list[int], i: int):
    if i >= len(t):
        return False

    return v == t[i] or appartient_optimisé(v, t, i + 1)