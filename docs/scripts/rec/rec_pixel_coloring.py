import tkinter as tk
import time

class Carré:

    def __init__(self, vec, couleur, texte = ''):
        self.texte = texte
        self.liste = []
        self.coord = vec #[(x0,y0), (x1,y1), (x2,y2), (x3,y3), ]
        self.couleur = couleur

    def changer_texte(self, texte):
        self.texte = texte

    def changer_texte(self, texte):
        self.liste = texte

    def ajouter_coord(self, L):
        self.coord = L

    def calculer_text_coord(self):
        x0, y0 = self.coord[0]
        x1, y1 = self.coord[2]
        return ((x0 + x1)/2, (y0 +y1)/2)

    def calculer_multi_text_coord(self, margins = -2):
        x0, y0 = self.coord[0]
        x1, y1 = self.coord[2]
        n = len(self.liste)
        if n >= 5:
            return [(x0+ abs(x0-x1)/4 + i % 3 * abs(x0-x1)/4, y0 + abs(y0-y1)/4 + i // 3 * abs(y0-y1)/4) for i in range(n)]
        else:
            return [(x0 + margins + abs(x0+margins-(x1-margins))/3 + i % 2 * abs(x0+margins-(x1-margins))/3, \
                     y0 + margins + abs(y0+margins-(y1-margins))/3 + i // 2 * abs(y0+margins-(y1-margins))/3) for i in range(n)]

class Damier(tk.Tk):

    DICO_COULEUR = {0:'white', 1:'black', 2:'red', 3:'green'}

    def __init__(self, plateau, L = 50):
        super().__init__()
        self.plateau = plateau
        self.nx = len(self.plateau)
        self.ny = len(self.plateau[0])
        self.canvas = tk.Canvas(self, width=L * self.ny+20, height=L * self.nx +20)
        motif_base = [(10, 10), (10+L, 10), (10+L, 10+L), (10, 10+L)]
        print([[self.DICO_COULEUR[int(self.plateau[i][j])] for i in range(self.nx)] for j in range(self.ny)])
        self.carrés = [[Carré([(x + L * j, y + L * i) for (x,y) in motif_base], self.DICO_COULEUR[int(self.plateau[i][j])]) for i in range(self.nx)] for j in range(self.ny)]

    def afficher_plateau(self, L = 50):
        for j in range(self.nx):
            for i in range(self.ny):
                self.canvas.create_polygon(self.carrés[i][j].coord, fill = self.carrés[i][j].couleur, outline='black')
        self.canvas.grid(row=0, column=0)

    def maj_plateau(self, j, i, couleur, t_sleep=0.5):
        self.carrés[i][j].couleur = self.DICO_COULEUR[int(couleur)]
        if couleur != "3":
            self.canvas.create_polygon(self.carrés[i][j].coord, fill = self.carrés[i][j].couleur, outline='black')
        else:
            self.canvas.create_line(self.carrés[i][j].coord[0], self.carrés[i][j].coord[2], fill = "white")
            self.canvas.create_line(self.carrés[i][j].coord[1], self.carrés[i][j].coord[3], fill = "white")
        self.canvas.update()
        time.sleep(2 * t_sleep)

# Dessin du départ à remplacer par une ouverture de fichier le contenant
# puis/ou une vraie image à terme
dessin_depart= [
"01111110",
"01000001",
"10000010",
"10001111",
"01100010",
"00011100",
]

def tableau_string_vers_tableau_tableau(dessin):
    Nb_lig=len(dessin_depart)
    Nb_col=len(dessin_depart[0])
    T = [["0" for i in range(Nb_col)] for j in range(Nb_lig)]
    for i in range(Nb_lig):
        for j in range(Nb_col):
            T[i][j] = dessin_depart[i][j]
    return T

def affiche(dessin):
    for i in range (len(dessin)):
#        print(end='"')
        for j in range (len(dessin[0])):
            if dessin[i][j] == '1':
                #print(dessin[i][j],end="")
                print(u'\u25a0',end="")
            elif dessin[i][j] == '2':
                print(u'\u25ce',end="")
            else:
                print(u'\u25a1',end="")
#        print(end='",\n')
        print(end='\n')
    print()
affiche(dessin_depart)


# Fonction récursive
def colorier(t, pos_i, pos_j):
    print(pos_i,pos_j)
    if t[pos_i][pos_j] != "0":
        pass
    else:
        pass
  

T = tableau_string_vers_tableau_tableau(dessin_depart)
damier = Damier(T)
tk.Button(text = "Démarrer", command = lambda x = 2 : colorier(T, 1, 4, 0, Damier(T))).pack()


tk.mainloop()
