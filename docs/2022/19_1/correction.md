Cette version n'est pas si évidente pour un élève : il faut bien voir dans le cas d'une double négation ce qui se passe (double appel récursif passant par chacun des `if`)

```python linenums='1'
def multiplication(n1, n2):
    if n1 < 0:
        return -multiplication(-n1, n2)
    if n2 < 0:
        return -multiplication(n1, -n2)
    resultat = 0
    for _ in range(n2):
        resultat += n1
    return resultat
```

Peut-être que cette version plus longue mais plus explicite sera celle trouvée par les candidats en réussite sur cet exercice pas facile :

```python
def multiplication(n1, n2):
    if n1 < 0 and n2 < 0:
        return multiplication(-n1, -n2)
    elif n1 < 0:
        return -multiplication(-n1, n2)
    elif n2 < 0:
        return -multiplication(n1, -n2)
    else:
        resultat = 0
        for _ in range(n2):
            resultat += n1
        return resultat
```