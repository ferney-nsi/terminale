import pygame # importation de la librairie pygame
#import space
import sys # pour fermer correctement l'application
import random

class Case:
    """ 
    Une case est représentée par ses coordonnées en haut à gauche
    Elle contient un joueur de la classe Joueur."""

    def __init__(self, i, j, joueur = None):
        """ les cases auront 3 types possibles: rouge (montagne), bleu (océan), vert (plaine) tirées aléatoirement"""
        pass

    def placer_joueur(self, joueur):
        """ place un joueur sur une case"""
        pass

class Plateau:

    def __init__(self, nx, ny, screen, L = 50, H = 50):
        """
        nx = nombre de lignes du plateau
        ny = nombre de colonnes
        L, H = largeur et hauteur des cases
        image est le widget généré par pygame lors de la création de la fenêtre
        un plateau est un ensemble de cases"""
        self.image = screen
        pass
    
    def modifier_joueur(self, liste_joueur):
        """ajoute des joueurs dans les cases du plateau """
        pass
                
    def afficher_plateau(self):
        """affiche une case avec pygame : a adapter"""
        pygame.draw.rect(self.image, 'red', pygame.Rect(100, 100, 100, 100),0)
        pygame.draw.rect(self.image, 'red', pygame.Rect(100, 100, 100, 100),2)
        pass

    def afficher_joueur(self):
        """affiche les joueurs sur l'écran (attention à afficher la bonne image à la bonne position!)"""
        pass

class Joueur:

    def __init__(self, i, j, image, L = 50, H = 50):
        """ Constructeur :
        self.x, self.y = indices de la case sur laquelle le joueur est situé
        self.image = image du joueur
        self.point_de_vie etc....
        """

        self.image = pygame.image.load(image)
        self.image = pygame.transform.scale(self.image, (L, H))
    
    def deplacer(self, dirX, dirY):
        """met à jour la position du joueur"""
        pass



pygame.init() 

screen = pygame.display.set_mode((800,600))
pygame.display.set_caption("Plateau") 

plateau = Plateau(10,10,screen)

### BOUCLE DE JEU  ###
running = True # variable pour laisser la fenêtre ouverte

while running : # boucle infinie pour laisser la fenêtre ouverte

    screen.fill('black')

    ### Gestion des événements  ###
    for event in pygame.event.get(): # parcours de tous les event pygame dans cette fenêtre
        if event.type == pygame.QUIT : # si l'événement est le clic sur la fermeture de la fenêtre
            running = False # running est sur False
            sys.exit() # pour fermer correctement
       
        dirX = 0
        dirY = 0

       # gestion du clavier
        if event.type == pygame.KEYDOWN : # si une touche a été tapée KEYUP quand on relache la touche
            if event.key == pygame.K_LEFT : # si la touche est la fleche gauche
                dirX = -1 # on déplace le vaisseau de 1 pixel sur la gauche
            if event.key == pygame.K_RIGHT : # si la touche est la fleche droite
                dirX = 1 # on déplace le vaisseau de 1 pixel sur la gauche
            if event.key == pygame.K_UP : # si la touche est la fleche gauche
                dirY = -1 # on déplace le vaisseau de 1 pixel sur la gauche
            if event.key == pygame.K_DOWN : # si la touche est la fleche droite
                dirY = 1 # on déplace le vaisseau de 1 pixel sur la gauche
        print(dirX, dirY)
        
    plateau.afficher_plateau()
        

        
    pygame.display.update() # pour ajouter tout changement à l'écran
