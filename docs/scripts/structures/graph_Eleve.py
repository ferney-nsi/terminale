from tkinter import *
from random import *
from math import sqrt, pi, cos, sin, acos, asin, atan2

class GrapheDico:

    def __init__(self):
        self.adj = {}

    def ajouteSommet(self, s):
        # Expliquer aux élèves ce qu'est un ensemble set() et quel est l'intérêt (logique ensembliste)
        if s not in self.adj:
            self.adj[s] = set()

    def ajouteArc(self, s1, s2):
        # Préciser aux élèves l'utilisation de add() pour les ensembles
        self.ajouteSommet(s1)
        self.ajouteSommet(s2)
        self.adj[s1].add(s2)

    def ajouteArcs(self, s1, s2list):
        # Préciser aux élèves l'utilisation de add() pour les ensembles
        self.ajouteSommet(s1)
        for s2 in s2list.split(','):
            self.ajouteSommet(s2)
            self.adj[s1].add(s2)

    def testArc(self, s1, s2):
        # test si un arc relie le sommet s1 au sommet s2
        return s2 in self.adj[s1]

    def listeSommets(self):
        return list(self.adj)

    def listeVoisins(self, s):
        return list(self.adj[s])

    def isDoubleArc(self, s1, s2):
        return self.testArc(s1,s2) and self.testArc(s2,s1)


# Ne pas toucher

class Application(Tk):
    def __init__(self, G, func, arrow = True, width = 500, height = 500):
        Tk.__init__(self)        # constructeur de la classe parente
        self.can = Canvas(self, width = width, height =height, bg ="white")
        self.can.pack(side =TOP, padx = 5, pady =5)
        self.can.update()
        self.a = {}
        self.flag = False
        self.arrow = arrow
        self.dessine(G, eval(func) )#self.generateRegular)
        Button(self, text = "Polygone Regulier", command = lambda x=3: self.dessine(g,self.generateRegular)).pack(side =LEFT)
        Button(self, text = "Grille", command = lambda x=3: self.dessine(g,self.generateGrid)).pack(side =LEFT)
    
    def dessine(self, G, func, coords = []):
        self.can.delete('all')
        listCoords = func(len(G.listeSommets()))
        for i,text in enumerate(G.listeSommets()):
            self.a[text] = graphViz(self.can, listCoords[i][0], listCoords[i][1], text, G.listeVoisins(text), self.arrow)
        for text in G.listeSommets():
            self.a[text].cercles(self.can,self.a[text].x,self.a[text].y, text)#, g.listeVoisins(text))
            for sommet in G.listeVoisins(text):
                if G.isDoubleArc(text, sommet): 
                    self.flag = not self.flag
                    self.a[text].line(self.can,self.a[text].x,self.a[text].y, self.a[sommet].x,self.a[sommet].y, self.flag)
                else: self.a[text].line(self.can,self.a[text].x,self.a[text].y, self.a[sommet].x,self.a[sommet].y)

    def generateRegular(self, n):
        w = self.can.winfo_width()
        h = self.can.winfo_height()        
        return [(w/2 + w/3 * cos(i*2*pi/n),  h/2 + h/3 * sin(i*2*pi/n)) for i in range(n)]

    def generateGrid(self, n):
        w = self.can.winfo_width()
        h = self.can.winfo_height()
        length = 75    
        return [(i*length % w+50,  j*length % h+50) for i in range((w-50) // length) for j in range((h-50) // length)]

class graphViz:

    def __init__(self, canev, x, y, text, voisins, arrow):
        self.canev, self.x, self.y, self.text, self.voisins, self.arrow = canev, x, y, text, voisins, arrow
        self.r = 15

    def cercles(self, canev, x, y, text):
        self.cercle(canev, x, y)
        self.canev.create_text(x, y, text=text)
        self.cercle(canev, x, y)

    def cercle(self, can, x, y):
        "dessin d'un cercle de rayon <r> en <x,y> dans le canevas <can>"
        can.create_oval(x-self.r, y-self.r, x+self.r, y+self.r)

    def line(self, can, x0, y0, x1, y1, flag=None):
        "dessin d'un cercle de rayon <r> en <x,y> dans le canevas <can>"
        decalage = self.Angle(x0, y0, x1, y1)
        if self.arrow == True: arrow = 'last'
        else: arrow ='none'
        if flag is None:
            can.create_line(x0+self.r*decalage[0], y0-self.r*decalage[1], x1-self.r*decalage[0], y1+self.r*decalage[1], arrow = arrow)
        else:
            A, _ = self.thirdPts(x0+self.r*decalage[0], y0-self.r*decalage[1], x1-self.r*decalage[0], y1+self.r*decalage[1])
            can.create_line(x0+self.r*decalage[0], y0-self.r*decalage[1], A, x1-self.r*decalage[0], y1+self.r*decalage[1], smooth=True, arrow = arrow)

    def Angle(self, x0, y0, x1, y1):
        return (x1-x0)/sqrt((x0-x1)**2+(y0-y1)**2), (y0-y1)/sqrt((x0-x1)**2+(y0-y1)**2)

    def thirdPts(self, x0,y0,x1,y1):
        xMid = (x0+x1)/2
        yMid = (y0+y1)/2
        t = atan2(y1 - y0, x1 - x0)
        return (xMid+15*sin(t), yMid-15*cos(t)), (xMid-15*sin(t), yMid+15*cos(t))

# Ne pas toucher



# PROGRAMME PRINCIPAL

g = GrapheDico()
# Exemple 1 du cours
# g.ajouteArcs('A','B,C')
# g.ajouteArcs('B','A,D')
# g.ajouteArc('C','A')
# g.ajouteArc('D','B')

# Exemple 2 du cours
g.ajouteArcs('A','C,D')
g.ajouteArc('B','A')
g.ajouteArcs('C','E,D')
g.ajouteArcs('D','A,B,C,E,F')
g.ajouteArc('E','F')
g.ajouteArcs('F','D')

# g.ajouteArcs('Alice','Bob','Cathy')
# #g.ajouteArc('Alice','Cathy')
# g.ajouteArc('Bob','Deon')
# g.ajouteArc('Kevin','Leon')
# g.ajouteArc('Leon','Alice')
# g.ajouteArc('Alice','Leon')
# g.ajouteArc('Patsy','Leon')
# g.ajouteArc('Gollum','Leon')
# g.ajouteSommet('Eleonore')


Application(g, 'self.generateRegular', arrow = True).mainloop()