# Annales du BAC

Cette section vous donne des exemples de sujet que vous pouvez faire pour vous entrainer.

!!! tip "Sujet Centres Étrangers Candidats Libres J2 2021"

    [21-NSIJ2G11](pdf/annales/21-NSIJ2G11.pdf)

!!! tip "Sujet Centres Étrangers Candidats Libres J1 2021"

	[21-NSIJ1G11](pdf/annales/21-NSIJ1G11.pdf)

!!! tip "Sujet Métropole Septembre J2 2021"

	[21-NSIJ2ME3](pdf/annales/21-Sujet_sept2_NSIJ2ME3.pdf)

!!! tip "Sujet Amérique du Nord 2021"

	[21-NSIJ1AN1](pdf/annales/spe-numerique-informatique-2021-amerique-nord-1-sujet-officiel.pdf)

!!! tip "Sujet Polynésie 2021"

	[21-NSIJ2PO1](pdf/annales/spe-numerique-informatique-2021-polynesie-2-sujet-officiel.pdf)

!!! tip "Sujet Métropole Candidats Libres J2"

	[21-NSIJ2ME2](pdf/annales/spe-numerique-informatique-2021-metro-cand-libre-2-sujet-officiel.pdf)

!!! tip "Sujet Métropole Candidats Libres J1"

	[21-NSIJ1ME2](pdf/annales/spe-numerique-informatique-2021-metro-cand-libre-1-sujet-officiel.pdf)

!!! tip "Sujet Métropole J2 2021 (épreuve annulée)"

	[21-NSIJ2ME1](pdf/annales/Sujet2-metropole-mars-2021.pdf)

!!! tip "Sujet Métropole J1 2021 (épreuve annulée)"

	[21-NSIJ1ME1](pdf/annales/Sujet1-metropole-mars-2021.pdf)
