D'accord avec F. Chambon renvoyer -1 pour une recherche d'indice lorsque l'élément est absent est une mauvaise idée. Choisir un nom un peu plus spécifique que le `recherche`  rencontré une bonne dizaine de fois.

L'énoncé modifié est directement inspiré de [l'exercice 1 du sujet 8](https://ens-fr.gitlab.io/nsi-pratique/ex1/08/sujet/) proposé par F. Chambon.