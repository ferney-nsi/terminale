def pile():
    """ crée de la pile vide """ 
    return []

def est_vide(p):
    """renvoie True si la pile est vide
     et False sinon"""
    return p == []

def empiler(p, x):
    """Ajoute l’élément x à la pile p"""
    p.append(x)

def depiler(p):
    """dépile et renvoie l’élément au sommet de la pile p""" 
    assert not est_vide(p), "Pile vide"
    return p.pop()


class Pile:
    """ classe Pile : 
    création d’une instance Pile avec à partir d'une liste """
    def __init__(self):
        "Initialisation d’une pile vide"
        self.L = []

    def est_vide(self):
        """teste si la pile est vide""" 
        return

    def depiler(self): 
        """dépile si la pile n'est pas vide"""

        return

    def empiler(self,x): 
        """empile"""

def vérifier_parenthésage(expr : str) -> bool:
    pass

def vérifier_parenthésage_crochets(expr : str) -> bool:
    pass
            


def evaluer_npi(expr : str) -> float:
    """
    npi = notation_polonaise_inversée
    (2 + 3) * 4 + 1    ----> 2 3 + 4 * 1 +
    
    (2 + 3) * (4 + 1)  <---- 2 3 + 4 1 + * 
    2 3 + 4 1 + *
    pile : 2                |
    pile : 2 3              |
    pile : 5                | op = + : 2+3 = 5
    pile : 5 4              | 
    pile : 5 4 1            | 
    pile : 5 5              | op = + : 4+1 = 5
    pile : 25               | op = * : 5*5 = 25
    renvoie le résultat : 25
    
    Il faut utiliser l'instruction **eval** : eval("3+3") donne 6 ou eval("abs(-7) + 3**2") donne 16
    """
    pass