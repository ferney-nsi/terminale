class Noeud:

    def __init__(self, v, g, d):
        self.gauche = g
        self.valeur = v
        self.droit = d

def initialiser_tableau_huffman(phrase):
    dico_occurrence = dict()
    for lettre in phrase:
        if lettre in dico_occurrence:
            dico_occurrence[lettre] += 1
        else:
            dico_occurrence[lettre] = 1
    tableau_arbre = [Noeud((clé, dico_occurrence[clé]), None, None) for clé in dico_occurrence]
    return tableau_arbre

def ordonner(tableau_arbre):
    for i in tableau_arbre:
        print(i.valeur)
    return sorted(tableau_arbre, key = lambda x:x.valeur[1])

def créer_arbre_huffman(phrase):
    pass

def encoder(arbre, code= "", dictionnaire = {}):
    pass

def compresser(phrase):
    pass

def affiche(A):
    if A is None: return
    print("[", end="")
    affiche(A.gauche)
    print(A.valeur[0], end="")
    affiche(A.droit)
    print("]", end="")
    
D = créer_arbre_huffman("code de morse")
affiche(D)
print(encoder(D))
print(compresser("code de morse"))