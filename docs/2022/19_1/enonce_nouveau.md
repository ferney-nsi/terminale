Programmer la fonction `multiplication`, prenant en paramètres deux nombres entiers
`n1` et `n2`, et qui renvoie le produit de ces deux nombres.
Les seules opérations autorisées sont l’addition et la soustraction. 

_Indication_ : On rappelle que si $a$ est un entier négatif et $b$ un entier alors on peut manipuler $-a$ qui sera positif et remplacer $a \times b$ par $-(-a \times b)$.

Exemples :
```python
>>> multiplication(3, 5)
15
>>> multiplication(-4, -8)
32
>>> multiplication(-2, 6)
-12
>>> multiplication(3, -4)
-12
>>> multiplication(-2, 0)
0
```